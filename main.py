from typing import List
import re
import requests
import lxml.html
import os


class KanoPazAutoPack():
    APP_STORE_URL = "https://play.google.com/store/apps/details?id=jp.enish.kanopazu&hl=ja&gl=US"
    BUNDLE_ENDPOINT = "https://www-falcon-jp.enish-games.com"
    RESOURCE_ENDPOINT = "https://assets.enish-games.com/assets-falcon/Resources/android/"
    USER_AGENT = "Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11A465 Safari/9537.53"

    def __init__(self, outputDir: str = "unity2d", outputUrlFile: str = "files.txt"):
        self.outputDir = outputDir
        self.outputUrlFile = outputUrlFile

    def download(self, useVersionSurfix=True):
        """
        インスタンス内のメソッドを呼び出し自動的にダウンロードする
        """
        # バージョン番号文字列を取得する
        versionString = self.getVersionString()
        if useVersionSurfix:
            outputFile = self.outputUrlFile.split('.')[0]
            version = versionString.replace('.', '_')
            self.outputUrlFile = outputFile + version + '.txt'
        # ファイル名リストを取得する
        fileNames = self.getResourceList(versionString)
        # リソースアドレスリストを作成する
        resourceAddresses = self.createResourceAddressList(fileNames)
        # ファイルにアドレスリストを保存する
        self.saveAddressesToFile(resourceAddresses)
        # リソースをダウンロードする
        self.downloadResources()

    def getVersionString(self) -> List[str]:
        """
        GooglePlayから最新のバージョン番号文字列を取得する
        """
        url = self.APP_STORE_URL
        resp = requests.get(url).text
        tree = lxml.html.fromstring(resp)
        version = tree.xpath("//span[@class='htlgb']/text()")[3]
        return version

    def getResourceList(self, versionString: str) -> List[str]:
        """
        AssetBundleを取得し、ファイル名リストを生成して返す
        """
        version = versionString.replace('.', '_')
        bundleUrl = f"{self.BUNDLE_ENDPOINT}/v{version}/resource/list/Android"
        resp = requests.get(bundleUrl).text
        if "メンテナンス中" in resp:
            raise Exception("Server is in maintenance")
        # 応答は application/protobufで返ってくるが ここでは正規表現でアドレスを取り出す
        fileNames = re.findall(r"[0-9a-f]{32}", resp.text)
        return fileNames

    def createResourceAddressList(self, fileNames: List[str]) -> List[str]:
        """
        リソースアドレスのリストを作成する
        既にフォルダに存在するリソースはスキップする(よくない仕様)
        """
        resourceAddresses = [
            self.RESOURCE_ENDPOINT + fileName
            for fileName in fileNames
            if not os.path.exists(os.path.join(self.outputDir, fileName))
        ]
        return resourceAddresses

    def saveAddressesToFile(self, resourceAddresses: List[str]) -> bool:
        """
        リソースアドレスをファイルに保存する
        """
        with open(self.outputUrlFile, 'w') as f:
            for address in resourceAddresses:
                f.write(address + '\r\n')
        return True

    def downloadResources(self):
        """
        リソースをwgetを呼びダウンロードする
        """
        os.system(
            " ".join([
                'wget',
                '--random-wait',
                '--no-check-certificate',
                '-c',
                '-N',
                f'--user-agent="{self.USER_AGENT}"',
                '-i',
                self.outputUrlFile,
                '-P',
                self.outputDir
            ])
        )


if __name__ == "__main__":
    # インスタンス作成
    cl = KanoPazAutoPack()
    # 自動ダウンロード
    cl.download()
