# license
 Developer:[gochiAI](https://gitlab.com/kanopaz/auto-pack/-/raw/main/LICENSE)
# Recent Updates
Updated the script.  
The script has been updated to make it easier for anyone to run.  
The update consolidates a series of actions into a single command.  
# usage
bash all.sh [ver1] [ver2]
# tools
bash diff.sh [commit1 or tag1] [commit2 or tag2]
